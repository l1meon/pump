#require 'rubygems'
require 'selenium-webdriver'
require 'headless'
#require 'pg'

#def database_connection(username, password, host, dbname)
#	@connection = PG.connect dbname: dbname, user: username, password: password, host: host
#end

#def get_ips_data(table_name)
#	@connection.exec "SELECT * FROM #{table_name}"
#end

def create_browser(title, ip)
	@user_agents = [
   'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2227.1 Safari/537.36',
   'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2227.0 Safari/537.36',
   'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:45.0) Gecko/20130401 Firefox/45.0',
   'Mozilla/5.0 (Windows NT 6.1; rv:45.0) Gecko/20100101 Firefox/45.0',
   'Mozilla/5.0 (X11; Linux i586; rv:45.0) Gecko/20100101 Firefox/45.0',
   'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:45.0) Gecko/20100401 Firefox/45.0'
  ]
  @agent = @user_agents.shuffle!.sample
  profile = Selenium::WebDriver::Firefox::Profile.new
  profile['browser.cache.disk.enable'] = false
  profile['browser.cache.memory.enable'] = false
  profile['browser.cache.offline.enable'] = false
	profile['network.http.use-cache'] = false
	profile['general.useragent.override'] = @agent

	@browser = Selenium::WebDriver.for :firefox, profile: profile
	@browser.manage.delete_all_cookies
	@browser.navigate.to 'http://google.com'
	ua = @browser.execute_script('return navigator.userAgent')
	p "User agent: #{ua}"

	@input = @browser.find_element(:name, 'q')
	stringify = "#{title} " + "youtube: Игры пк"
	@input.send_keys(stringify)
	p "Finding the video: #{title}"
	@button = @browser.find_element(:class, 'lsb')
	@button.click
	sleep(3)

	@link = @browser.find_element(:partial_link_text, title)
	@link.click
	sleep(5)
	p @browser.title
	@browser.save_screenshot("starting/begin_#{title}_#{ip}.png")
	random = Random.new.rand(32..79)
	sleep(random/2)
	@browser.save_screenshot("screens/watching_#{title}_#{ip}.png")
	sleep(random/2)
	waiting_for(2)
	time = Time.now
	@browser.save_screenshot("ending/end_#{title}_#{ip}.png")
	puts "#{time.hour}h:#{time.min}m:#{time.sec}s"
	@browser.manage.delete_all_cookies
	#@browser.quit
end

def waiting_for(time)
	@changed = false
	@future = Time.now + time
	while @changed == false
		if @future <= Time.now
			@changed = true
		else
			@changed = false
		end
	end
end

def updated_ips
	@updated ||= []
	IO.foreach('working_ips.out') do |line|
		@updated << line.chomp
	end
	IO.foreach('working_60sec.out') do |line|
		@updated << line.chomp
	end

	@updated.uniq.shuffle!
end

	def used_ips
		@used_ips ||= []
		IO.foreach('used_ips.out') do |line|
			@used_ips << line.chomp
		end

		@used_ips.uniq
	end

	def randomized_ips
		random = Random.new.rand(5..31)
		random.times do
			updated_ips.shuffle!
		end
		updated_ips
	end

	randomized_ips.each do |ip_address|
		unless used_ips.include?(ip_address)
			# вариант с смены роутом соурс ип
			# system "echo 'skildrote' | sudo -S ip -6 route del default"
			# system "echo 'skildrote' | sudo -S ip -6 route add default via ipv6_gateway src ipv6_address"
			# вариант с дизэйблэм ип
			# system "echo 'skildrote' | sudo -S ip -6 addr change ip_address/64 dev eth0 preferred_lft forever"
			# вариант с иптаблес
			p "Adding ip to ip6tables SNAT"
			system "echo 'skildrote' | sudo -S ip6tables -t nat -A POSTROUTING -o eth0 -j SNAT --to-source #{ip_address}"
			sleep(60)
			system "curl ifconfig.co"
			p "--- Begin webdriver ---"
			@headless = Headless.new(:video => {:provider => :ffmpeg})
			@headless.start
			#unless File.exists?('/videos/test_video.mov')
			#	@headless.video.start_capture
			#	create_browser('Читер в CS GO ПАТРУЛЬ #', ip_address)
			#	@headless.video.stop_and_save("/home/deploy/main_script/v1/videos/test_video.mov")
			#end
			create_browser('Читер в CS GO ПАТРУЛЬ #', ip_address)
			p "--- End webdriver ---"
			@headless.destroy
			open('used_ips.out', 'a') do |f|
				f.puts ip_address
			end
			used_ips << ip_address
			p "Deleting ip from ip6tables"
			system "echo 'skildrote' | sudo -S ip6tables -t nat -D POSTROUTING 1"
		end
		p "Ip deleted getting to next..."
		p ":::::::::::::::::::::::::::::::::::"
	end
